$(document).ready(function () {
  var owl1 = $("#owl-slider");

  owl1.owlCarousel({
    loop: true,
    autoplay: true,
    autoplayTimeout: 15000,
    nav: false,
    dots: true,
    responsive: {
      0: {
        items: 1,
      },
    },
  });
});
