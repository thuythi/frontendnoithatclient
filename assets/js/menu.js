var dropMenu = document.getElementsByClassName("drop-menu-mobile");
var liItem = document.getElementsByClassName("mobile-menu-item");
for (var i = 0; i < dropMenu.length; i++) {
  dropMenu[i].addEventListener("click", function () {
    for (var j = 0; j < liItem.length; j++) {
      liItem[j].classList.remove("active");
    }
    for (var j = 0; j < dropMenu.length; j++) {
      if (dropMenu[j] != this) {
        dropMenu[j]
          .closest(".list-items")
          .nextElementSibling.classList.remove("active");
      }
      dropMenu[j].classList.remove("active");
    }
    this.classList.toggle("active");
    this.closest(".mobile-menu-item").classList.add("active");
    this.closest(".list-items").nextElementSibling.classList.toggle("active");
  });
}

//
var close = document.getElementsByClassName("close-modal");
for (var i = 0; i < close.length; i++) {
  close[i].addEventListener("click", function () {
    console.log(this);
    this.closest(".modal1").classList.remove("active");
    document.getElementById("opacity").classList.remove("active");
  });
}
//
document.getElementById("menu-mobile").addEventListener("click", function () {
  document.getElementById("opacity").classList.add("active");
  this.nextElementSibling.classList.add("active");
});
