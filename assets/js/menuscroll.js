window.onscroll = function () {
  if (document.body.scrollTop > 0 || document.documentElement.scrollTop > 0) {
    document
      .getElementsByClassName("menu-top-header")[0]
      .classList.add("header-fixed");
  } else {
    document
      .getElementsByClassName("menu-top-header")[0]
      .classList.remove("header-fixed");
  }
};
